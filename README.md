# Composable Dotfiles and Scripts Collection

*Canonical source is <https://gitlab.com/rwxrob/dotfiles>*

![WIP](https://img.shields.io/badge/status-wip-red)

WARNING: This thing is *never* complete, full of bugs, and always changing. [Pilfer](https://duck.com/lite?q=Pilfer) at your own risk. But *do* pilfer.

